const io = require('socket.io')(3000),
    cfg = require('./config.json'),
    mongoose = require('mongoose'),
    natural = require('natural'),
    fs = require('fs'),
    User = require('./models/user.js'),
    Category = require('./models/category.js'),
    express = require('express'),
    htmlToText = require('html-to-text'),
    request = require('request');
    
    

function getText(link) {

    
}
    

    mongoose.connect(cfg['mongo_url'], function(err) {
        if (err) throw err;
        console.log("Connected to URL: " + cfg['mongo_url']);
        io.on('connection', function (socket) {
            socket.emit('hello', { from: 0, msg: 'Привет. Я помогу тебе найти интересные и важные для тебя ресурс. Сейчас гляну только, что у Вас в истории-с и в закладках.'});
            socket.on('hist', function (data) {
              var user = new User(data);
              user.save((err, doc) => {
                  console.log(err);
                  console.log(doc);
                  
              })
            });
            socket.on('addtree', function(data) {
                var newCategory = new Category(data)
                console.log(data)
                newCategory.save(function(err, doc) {
                    console.log(doc)
                })
            });

            function train(link, cat) {
                request(link, function(err,resp, body){
                    var text = htmlToText.fromString(body, {
                        ignoreHref: true
                    });
                    natural.BayesClassifier.load('./neural/classifier.json', null, function(err, classifier) {
                        classifier.addDocument(text, cat);
                        classifier.train();
                        classifier.save('./neural/classifier.json', function(err, classifier) {
                            socket.emit('trained', {
                                from: 0,
                                msg: "Добавлено как " + cat
                            })
                        });
                    });
                })  
            }

            function classifyLink(link) {
                request(link, function(err,resp, body){
                    var text = htmlToText.fromString(body, {
                        ignoreHref: true
                    });
                    natural.BayesClassifier.load('./neural/classifier.json', null, function(err, classifier) {
                        var cls = classifier.classify(text);
                        console.log(cls);
                        
                        socket.emit('classified', {
                            from: 0,
                            msg: "Тематика сайта: " + cls + ". Напишити 'add' чтобы добавить в эту категорию или напишите rate [0-9] чтобы оценить добавленный ресурс",
                            extra: classifier.getClassifications(text)
                        });
                    });
                })               

                }


            socket.on('message', function(data) {
                var msg = data.msg.trim().split(' ');
                var user = data.user;
                
                console.log("BBOT Client:... ", msg);
                if (msg[0] == "train") {
                    if (msg.length >=5) {
                    train(msg[2], msg[4])
                    console.log('here');
                    }
                }
                if (msg[0] == "classify") {
                    if (msg.length >=3) {
                        classifyLink(msg[2])
                    }
                    
                }


          });
    });



    })