var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    user: String,
    bookmarks: Object,
    history: Object,
    online: Boolean,
    balance: Number
});

module.exports = mongoose.model('User', userSchema);