var mongoose = require('mongoose');

var catSchema = mongoose.Schema({
    user: String,
    tree: Object
});

module.exports = mongoose.model('Category', catSchema);